class GenexModal {

    constructor() {
        this.setDefaultConfiguration();
    }

    setDefaultConfiguration() {
        this._configuration = { 
            parentElement: document.body,
            baseUrl: 'genexproxy/',
            sessionStorageKey: 'genex-eventInformation-',
            backdropClickClose: true,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            // classes
            backdropClass: 'genex-backdrop',
            modalClass: 'genex-survey-modal',
            modalCloseClass: 'genex-survey-modal-close',
            modalIFrameClass: 'genex-survey-modal-iframe',
            // callbacks
            onTriggerResultEvent: null,
            onBeforeSurveyStart: null,
            onSurveyStarted: null,
            onSurveyClosed: null,
            onError: null
        };
    };

    setConfiguration(configuration) {
        if (!this._configuration) {
            this.setDefaultConfiguration();
        }

        if (!configuration) {
            console.warn('Parameter cannot be null: configuration. Default configuration used');
        }

        for(let property in this._configuration) {
            if (property in configuration) {
                this._configuration[property] = configuration[property];
            }
        }

        if (this._configuration.baseUrl.length > 1 && !this._configuration.baseUrl.endsWith('/')) {
            this._configuration.baseUrl+= '/';
        }
    } 

    loadTrigger(uniqueInteractionId, payload, startSurvey = false) {
        let self = this;
        let eventInformation = GenexModal._newEventInformation(this._configuration.sessionStorageKey, uniqueInteractionId);
        if (!eventInformation) {
            let errorMessage = 'Cannot create another Event with the same unique Interaction Id ' + uniqueInteractionId
            console.error(errorMessage);
            if (this._configuration.onError) {
                this._configuration.onError(uniqueInteractionId, eventInformation, errorMessage);
            }
            return new Promise(() => { return null; });
        }
        eventInformation.payload = payload;
        GenexModal._setEventInformation(this._configuration.sessionStorageKey, uniqueInteractionId, eventInformation);
        let url = this._configuration.baseUrl + 'trigger/query';
        return fetch(url, {
            method: 'POST',
            cache: 'no-cache',
            headers: this._configuration.headers,
            body: JSON.stringify(eventInformation.payload)
        })
        .then(response => response.json())
        .then((data) => {
            console.log('Trigger Response:', data);
            return self._handleTriggerQueryResponse(uniqueInteractionId, data, startSurvey);
        })
        .catch((error) => {
            let errorMessage = 'Trigger Response Error: ' + JSON.stringify(error);
            console.error(errorMessage);
            if (this._configuration.onError) {
                this._configuration.onError(uniqueInteractionId, eventInformation, errorMessage);
            }
            return self._handleTriggerQueryResponse(uniqueInteractionId);
        });
    }

    canSurvey(uniqueInteractionId) {
        let eventInformation = GenexModal._getEventInformation(this._configuration.sessionStorageKey, uniqueInteractionId);
        if (!eventInformation || !eventInformation.surveyId) {
            return false;
        }
        return !eventInformation.surveyActioned;
    }
    
    startSurvey(uniqueInteractionId, payload = null) {
        let self = this;
        let eventInformation = GenexModal._getEventInformation(this._configuration.sessionStorageKey, uniqueInteractionId);
        if (!eventInformation) {
            return this.loadTrigger(uniqueInteractionId, payload, true);
        }
        if (!this.canSurvey(uniqueInteractionId)) {
            let errorMessage = 'Cannot Survey';
            console.error(errorMessage);
            if (this._configuration.onError) {
                this._configuration.onError(uniqueInteractionId, eventInformation, errorMessage);
            }
            return new Promise(() => { return null; });
        }
        if (payload) {
            eventInformation.payload = payload;   
        }
        eventInformation.payload.survey_id = eventInformation.surveyId;
        eventInformation.surveyActioned = true;
        GenexModal._setEventInformation(this._configuration.sessionStorageKey, uniqueInteractionId, eventInformation);
        
        let url = this._configuration.baseUrl + 'participant/';
        return fetch(url, {
            method: 'POST',
            cache: 'no-cache',
            headers: this._configuration.headers,
            body: JSON.stringify(eventInformation.payload)
        })
        .then(response => response.json())
        .then((data) => {
            console.log('Participant Response:', data);
            return self._handleParticipantResponse(uniqueInteractionId, data);
        })
        .catch((error) => {
            console.error('Participant Response Error:', error);
            return self._handleParticipantResponse(uniqueInteractionId);
        });
    }

    _handleTriggerQueryResponse(uniqueInteractionId, data = null, startSurvey = false) {
        let eventInformation = GenexModal._getEventInformation(this._configuration.sessionStorageKey, uniqueInteractionId);
        let errors = data && data.errors ? data.errors : null;
        eventInformation.triggerQueried = true;
        eventInformation.surveyId = data && data.result_ok ? data.survey_id : null;
        GenexModal._setEventInformation(this._configuration.sessionStorageKey, uniqueInteractionId, eventInformation);
        
        if (this._configuration.onTriggerResultEvent) {
            this._configuration.onTriggerResultEvent(uniqueInteractionId, eventInformation, errors);
        }

        if (startSurvey) {
            return this.startSurvey(uniqueInteractionId);
        }

        return eventInformation;
    }

    static _formatLink(link) {
        if (link.toLowerCase().startsWith("http://") || link.toLowerCase().startsWith("https://")) {
            return link;
        }
        return 'https://' + link;
    }

    _handleParticipantResponse(uniqueInteractionId, data = null) {
        let eventInformation = GenexModal._getEventInformation(this._configuration.sessionStorageKey, uniqueInteractionId);
        if (data && data.result_ok) {
            eventInformation.link = GenexModal._formatLink(data.link);
        }
        GenexModal._setEventInformation(this._configuration.sessionStorageKey, uniqueInteractionId, eventInformation);
        if (this._configuration.onBeforeSurveyStart) {
            this._configuration.onBeforeSurveyStart(uniqueInteractionId, eventInformation);
        }
        this._handleSurveyModal(uniqueInteractionId, eventInformation);
        return eventInformation;
    }

    loadIframe(uniqueInteractionId, url) {
        let eventInformation = {
            link: url
        };
        this._handleSurveyModal(uniqueInteractionId, eventInformation);
    }

    _handleSurveyModal(uniqueInteractionId, eventInformation) {
        let self = this;
        let parentDiv = this._configuration.parentElement;
        let rootDiv = document.createElement('div');
        // backdrop
        let backdropDiv = document.createElement('div');
        backdropDiv.classList.add(this._configuration.backdropClass);
        if (this._configuration.backdropClickClose) {
            backdropDiv.addEventListener('click', (event) => {
                self._handleModalClose(uniqueInteractionId, rootDiv, event);
            });
        }
        rootDiv.appendChild(backdropDiv);
        // modal
        let modalDiv = document.createElement('div');
        modalDiv.classList.add(this._configuration.modalClass);
        rootDiv.appendChild(modalDiv);
        // modal close button
        let modalClose = document.createElement('div');
        modalClose.classList.add(this._configuration.modalCloseClass);
        modalClose.addEventListener('click', (event) => {
            self._handleModalClose(uniqueInteractionId, rootDiv, event);
        });
        modalDiv.appendChild(modalClose);
        // modal iframe
        let modalIFrame = document.createElement('iframe');
        modalIFrame.classList.add(this._configuration.modalIFrameClass);
        modalIFrame.setAttribute('border', 0);
        modalIFrame.setAttribute('src', eventInformation.link);
        window.addEventListener('message', (event) => {
            if (document.body.contains(rootDiv) && event.data === 'close-genex-modal') {
                self._handleModalClose(uniqueInteractionId, rootDiv, event);
            }
        });
        modalDiv.appendChild(modalIFrame);
        
        parentDiv.insertBefore(rootDiv, parentDiv.firstChild);

        if (this._configuration.onSurveyStart) {
            this._configuration.onSurveyStart(eventInformation);
        }
    }

    _handleModalClose(uniqueInteractionId, rootDiv, event) {
        let eventInformation = GenexModal._getEventInformation(this._configuration.sessionStorageKey, uniqueInteractionId);
        let parentDiv = this._configuration.parentElement;
        parentDiv.removeChild(rootDiv);

        if (this._configuration.onSurveyClosed) {
            this._configuration.onSurveyClosed(uniqueInteractionId, eventInformation);
        }
    }

    static _newEventInformation(sessionStorageKey, uniqueInteractionId) {
        let sessionKey = sessionStorageKey + uniqueInteractionId;
        if (sessionStorage.getItem(sessionKey)) {
            return null;
        }
        let eventInformation = {
            triggerQueried: false,
            surveyActioned: false
        };
        GenexModal._setEventInformation(sessionStorageKey, uniqueInteractionId, eventInformation);
        return eventInformation;
    }

    static _setEventInformation(sessionStorageKey, uniqueInteractionId, eventInformation) {
        let sessionKey = sessionStorageKey + uniqueInteractionId;
        sessionStorage.setItem(sessionKey, JSON.stringify(eventInformation));
    }

    static _getEventInformation(sessionStorageKey, uniqueInteractionId) {
        let sessionKey = sessionStorageKey + uniqueInteractionId;
        if (!sessionStorage.getItem(sessionKey)) {
            return null;
        }
        let eventInformation = JSON.parse(sessionStorage.getItem(sessionKey));
        return eventInformation;
    }
}
