"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var GenexModal = /*#__PURE__*/function () {
  function GenexModal() {
    _classCallCheck(this, GenexModal);

    this.setDefaultConfiguration();
  }

  _createClass(GenexModal, [{
    key: "setDefaultConfiguration",
    value: function setDefaultConfiguration() {
      this._configuration = {
        parentElement: document.body,
        baseUrl: 'genexproxy/',
        sessionStorageKey: 'genex-eventInformation-',
        backdropClickClose: true,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        // classes
        backdropClass: 'genex-backdrop',
        modalClass: 'genex-survey-modal',
        modalCloseClass: 'genex-survey-modal-close',
        modalIFrameClass: 'genex-survey-modal-iframe',
        // callbacks
        onTriggerResultEvent: null,
        onBeforeSurveyStart: null,
        onSurveyStarted: null,
        onSurveyClosed: null,
        onError: null
      };
    }
  }, {
    key: "setConfiguration",
    value: function setConfiguration(configuration) {
      if (!this._configuration) {
        this.setDefaultConfiguration();
      }

      if (!configuration) {
        console.warn('Parameter cannot be null: configuration. Default configuration used');
      }

      for (var property in this._configuration) {
        if (property in configuration) {
          this._configuration[property] = configuration[property];
        }
      }

      if (this._configuration.baseUrl.length > 1 && !this._configuration.baseUrl.endsWith('/')) {
        this._configuration.baseUrl += '/';
      }
    }
  }, {
    key: "loadTrigger",
    value: function loadTrigger(uniqueInteractionId, payload) {
      var _this = this;

      var startSurvey = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      var self = this;

      var eventInformation = GenexModal._newEventInformation(this._configuration.sessionStorageKey, uniqueInteractionId);

      if (!eventInformation) {
        var errorMessage = 'Cannot create another Event with the same unique Interaction Id ' + uniqueInteractionId;
        console.error(errorMessage);

        if (this._configuration.onError) {
          this._configuration.onError(uniqueInteractionId, eventInformation, errorMessage);
        }

        return new Promise(function () {
          return null;
        });
      }

      eventInformation.payload = payload;

      GenexModal._setEventInformation(this._configuration.sessionStorageKey, uniqueInteractionId, eventInformation);

      var url = this._configuration.baseUrl + 'trigger/query';
      return fetch(url, {
        method: 'POST',
        cache: 'no-cache',
        headers: this._configuration.headers,
        body: JSON.stringify(eventInformation.payload)
      }).then(function (response) {
        return response.json();
      }).then(function (data) {
        console.log('Trigger Response:', data);
        return self._handleTriggerQueryResponse(uniqueInteractionId, data, startSurvey);
      })["catch"](function (error) {
        var errorMessage = 'Trigger Response Error: ' + JSON.stringify(error);
        console.error(errorMessage);

        if (_this._configuration.onError) {
          _this._configuration.onError(uniqueInteractionId, eventInformation, errorMessage);
        }

        return self._handleTriggerQueryResponse(uniqueInteractionId);
      });
    }
  }, {
    key: "canSurvey",
    value: function canSurvey(uniqueInteractionId) {
      var eventInformation = GenexModal._getEventInformation(this._configuration.sessionStorageKey, uniqueInteractionId);

      if (!eventInformation || !eventInformation.surveyId) {
        return false;
      }

      return !eventInformation.surveyActioned;
    }
  }, {
    key: "startSurvey",
    value: function startSurvey(uniqueInteractionId) {
      var payload = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var self = this;

      var eventInformation = GenexModal._getEventInformation(this._configuration.sessionStorageKey, uniqueInteractionId);

      if (!eventInformation) {
        return this.loadTrigger(uniqueInteractionId, payload, true);
      }

      if (!this.canSurvey(uniqueInteractionId)) {
        var errorMessage = 'Cannot Survey';
        console.error(errorMessage);

        if (this._configuration.onError) {
          this._configuration.onError(uniqueInteractionId, eventInformation, errorMessage);
        }

        return new Promise(function () {
          return null;
        });
      }

      if (payload) {
        eventInformation.payload = payload;
      }

      eventInformation.payload.survey_id = eventInformation.surveyId;
      eventInformation.surveyActioned = true;

      GenexModal._setEventInformation(this._configuration.sessionStorageKey, uniqueInteractionId, eventInformation);

      var url = this._configuration.baseUrl + 'participant/';
      return fetch(url, {
        method: 'POST',
        cache: 'no-cache',
        headers: this._configuration.headers,
        body: JSON.stringify(eventInformation.payload)
      }).then(function (response) {
        return response.json();
      }).then(function (data) {
        console.log('Participant Response:', data);
        return self._handleParticipantResponse(uniqueInteractionId, data);
      })["catch"](function (error) {
        console.error('Participant Response Error:', error);
        return self._handleParticipantResponse(uniqueInteractionId);
      });
    }
  }, {
    key: "_handleTriggerQueryResponse",
    value: function _handleTriggerQueryResponse(uniqueInteractionId) {
      var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var startSurvey = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

      var eventInformation = GenexModal._getEventInformation(this._configuration.sessionStorageKey, uniqueInteractionId);

      var errors = data && data.errors ? data.errors : null;
      eventInformation.triggerQueried = true;
      eventInformation.surveyId = data && data.result_ok ? data.survey_id : null;

      GenexModal._setEventInformation(this._configuration.sessionStorageKey, uniqueInteractionId, eventInformation);

      if (this._configuration.onTriggerResultEvent) {
        this._configuration.onTriggerResultEvent(uniqueInteractionId, eventInformation, errors);
      }

      if (startSurvey) {
        return this.startSurvey(uniqueInteractionId);
      }

      return eventInformation;
    }
  }, {
    key: "_handleParticipantResponse",
    value: function _handleParticipantResponse(uniqueInteractionId) {
      var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      var eventInformation = GenexModal._getEventInformation(this._configuration.sessionStorageKey, uniqueInteractionId);

      if (data && data.result_ok) {
        eventInformation.link = GenexModal._formatLink(data.link);
      }

      GenexModal._setEventInformation(this._configuration.sessionStorageKey, uniqueInteractionId, eventInformation);

      if (this._configuration.onBeforeSurveyStart) {
        this._configuration.onBeforeSurveyStart(uniqueInteractionId, eventInformation);
      }

      this._handleSurveyModal(uniqueInteractionId, eventInformation);

      return eventInformation;
    }
  }, {
    key: "loadIframe",
    value: function loadIframe(uniqueInteractionId, url) {
      var eventInformation = {
        link: url
      };

      this._handleSurveyModal(uniqueInteractionId, eventInformation);
    }
  }, {
    key: "_handleSurveyModal",
    value: function _handleSurveyModal(uniqueInteractionId, eventInformation) {
      var self = this;
      var parentDiv = this._configuration.parentElement;
      var rootDiv = document.createElement('div'); // backdrop

      var backdropDiv = document.createElement('div');
      backdropDiv.classList.add(this._configuration.backdropClass);

      if (this._configuration.backdropClickClose) {
        backdropDiv.addEventListener('click', function (event) {
          self._handleModalClose(uniqueInteractionId, rootDiv, event);
        });
      }

      rootDiv.appendChild(backdropDiv); // modal

      var modalDiv = document.createElement('div');
      modalDiv.classList.add(this._configuration.modalClass);
      rootDiv.appendChild(modalDiv); // modal close button

      var modalClose = document.createElement('div');
      modalClose.classList.add(this._configuration.modalCloseClass);
      modalClose.addEventListener('click', function (event) {
        self._handleModalClose(uniqueInteractionId, rootDiv, event);
      });
      modalDiv.appendChild(modalClose); // modal iframe

      var modalIFrame = document.createElement('iframe');
      modalIFrame.classList.add(this._configuration.modalIFrameClass);
      modalIFrame.setAttribute('border', 0);
      modalIFrame.setAttribute('src', eventInformation.link);
      window.addEventListener('message', function (event) {
        if (document.body.contains(rootDiv) && event.data === 'close-genex-modal') {
          self._handleModalClose(uniqueInteractionId, rootDiv, event);
        }
      });
      modalDiv.appendChild(modalIFrame);
      parentDiv.insertBefore(rootDiv, parentDiv.firstChild);

      if (this._configuration.onSurveyStart) {
        this._configuration.onSurveyStart(eventInformation);
      }
    }
  }, {
    key: "_handleModalClose",
    value: function _handleModalClose(uniqueInteractionId, rootDiv, event) {
      var eventInformation = GenexModal._getEventInformation(this._configuration.sessionStorageKey, uniqueInteractionId);

      var parentDiv = this._configuration.parentElement;
      parentDiv.removeChild(rootDiv);

      if (this._configuration.onSurveyClosed) {
        this._configuration.onSurveyClosed(uniqueInteractionId, eventInformation);
      }
    }
  }], [{
    key: "_formatLink",
    value: function _formatLink(link) {
      if (link.toLowerCase().startsWith("http://") || link.toLowerCase().startsWith("https://")) {
        return link;
      }

      return 'https://' + link;
    }
  }, {
    key: "_newEventInformation",
    value: function _newEventInformation(sessionStorageKey, uniqueInteractionId) {
      var sessionKey = sessionStorageKey + uniqueInteractionId;

      if (sessionStorage.getItem(sessionKey)) {
        return null;
      }

      var eventInformation = {
        triggerQueried: false,
        surveyActioned: false
      };

      GenexModal._setEventInformation(sessionStorageKey, uniqueInteractionId, eventInformation);

      return eventInformation;
    }
  }, {
    key: "_setEventInformation",
    value: function _setEventInformation(sessionStorageKey, uniqueInteractionId, eventInformation) {
      var sessionKey = sessionStorageKey + uniqueInteractionId;
      sessionStorage.setItem(sessionKey, JSON.stringify(eventInformation));
    }
  }, {
    key: "_getEventInformation",
    value: function _getEventInformation(sessionStorageKey, uniqueInteractionId) {
      var sessionKey = sessionStorageKey + uniqueInteractionId;

      if (!sessionStorage.getItem(sessionKey)) {
        return null;
      }

      var eventInformation = JSON.parse(sessionStorage.getItem(sessionKey));
      return eventInformation;
    }
  }]);

  return GenexModal;
}();
