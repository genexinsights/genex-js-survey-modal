# genex-js-survey-modal #

genex-js-survey-modal is a JavaScript library to manage browser based trigger events that may result in a modal popup contianing an iframe loading a web based survey hosted in the genex insights platform.

Session storage is used to track triggers queries against survey requests.

This project was writen using ES6 however contains transpiled versions of the library to ES5 and includes both a minified and obfuscated version.

---

## What you need to build genex-js-survey-modal

To build genex-js-survey-modal you need to have NodeJS Version v14.15.4 or higher installed as wel as a node package manager. NPM was used in the development of this library

1. run: npm install
2. run: npm run build

The last step will build the project to the */lib* folder
---

## Libraries (/lib)

- genex-js-survey-modal_*{version number}*.js - ES5 transpiled
- genex-js-survey-modal_*{version number}*.min.js - ES5 transpiled and minified
- genex-js-survey-modal_*{version number}*.oc.js - ES5 transpiled obfuscated using [javascript-obfuscator](https://www.npmjs.com/package/javascript-obfuscator)
- genex-js-survey-modal_*{version number}*.org.js - ES6 version (Original, no transpilation) 
---

## How use this project

you can either download the required library or alternatively use this project as part of your application.

genex-js-survey-modal contains the *GenexModal* class. 

### Configuration

The *GenexModal* class requires configuration to be set using the ```javascript setDefaultConfiguration(configuration) ``` method before any other calls are made.

Example of the full default configuration:
```javascript
{ 
    parentElement: document.body,
    baseUrl: 'genexproxy/',
    sessionStorageKey: 'genex-eventInformation-',
    backdropClickClose: true,
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    },
    // classes
    backdropClass: 'genex-backdrop',
    modalClass: 'genex-survey-modal',
    modalCloseClass: 'genex-survey-modal-close',
    modalIFrameClass: 'genex-survey-modal-iframe',
    // callbacks
    onTriggerResultEvent: null,
    onBeforeSurveyStart: null,
    onSurveyStarted: null,
    onSurveyClosed: null,
    onError: null
}
```

Example configuration:
```javascript
{
        parentElement: document.body,
        baseUrl: 'http://mysite/genexproxy',        
        backdropClickClose: false,
    }
```

####Configuration Options

| Property | Default | Description |
|---|---|---|
| parentElement | ```javascript document.body ``` | The parent element to which the modal element will be attached |
| baseUrl | ```javascript 'genexproxy/' ``` | the url to use as a base for the api calls. This can be a relative path |
| sessionStorageKey | ```javascript 'genex-eventInformation-' ``` | session storage prefix used |
| backdropClickClose | ```javascript true ``` | if true, clicking the backdrop will result in closing the modal |
| headers | ```javascript { 'Content-Type': 'application/json', 'Accept': 'application/json' } ``` | can be overriden to contain header elements in the form of a json object |
| backdropClass | ```javascript 'genex-backdrop' ``` | class name used for the backdrop element |
| modalClass | ```javascript 'genex-survey-modal' ``` | class name used for the modal element |
| modalCloseClass | ```javascript 'genex-survey-modal-close' ``` | class name used for the modal close button element |
| modalIFrameClass | ```javascript 'genex-survey-modal-iframe' ``` | class name used for the iframe element |
| onTriggerResultEvent | ```javascript  null, ``` | (option)call back method called when a trigger result is recevied |
| onBeforeSurveyStart | ```javascript  null ``` | (option)call back method called before a survey is started |
| onSurveyStarted | ```javascript  null ``` | (option)call back method called when a survey is started and the modal opened |
| onSurveyClosed | ```javascript  null ``` | (option)call back method called when the modal is closed |
| onError | ```javascript null ``` | (option)call back method called when an error occurs |

---

### The loadTrigger method (optional)
The ```javascript loadTrigger() ``` method will call the */trigger/query* endpoint to determine if an action will result in a survey. The result is storaged in session storage and managed by the *GenexModal* class.

Calling this method is optional. If this method is not called before the ```javascript startSurvey() ``` method, the */trigger/query* endpoint will be called before attempting to display the survey modal.

This method returns a Promise object

Example usage:
```javascript
modal.loadTrigger(uniqueInteractionId, payload);
```

| Parameter | Type | Description |
|---|---|---|
| uniqueInteractionId | string | a unique value representing the user's interaction. This value is to be used in the subsequent call to the *startSurvey* method for corelation purposes and is used to store the trigger query result against |
| payload | no default | the payload sent to the proxy or genex api. For reference to this strcuture, consult both the genex api and provided Source To Target Mapping |`
---

### The startSurvey method
The ```javascript startSurvey() ```  method will call the */participant/* endpoint to show the modal if the supplied payload is to result in a survey being served to the user.

This method returns a Promise object

Example usage:
```javascript
modal.startSurvey(uniqueInteractionId, payload);
```

| Parameter | Type | Description |
|---|---|---|
| uniqueInteractionId | string | a unique value representing the user's interaction. This value is to be used in the subsequent call to the *startSurvey* method for corelation purposes and is used to store the trigger query result against |
| payload | no default | the payload sent to the proxy or genex api. For reference to this strcuture, consult both the genex api and provided Source To Target Mapping |

---

### Full Example usage
```javascript 
let modal = new GenexModal();
modal.setConfiguration({
    parentElement: document.body,
    baseUrl: 'http://mysite/genexproxy',

    onSurveyClosed: (uniqueInteractionId, eventInformation) => {

        // place logic after survey closed here

    }
});

let uniqueInteractionId = new Date().getTime();
let payload = {
    data: "samplepayload"
};

// optional load trigger 
modal.loadTrigger(uniqueInteractionId, payload);

// call to start the survey modal if the payload results in a survey
modal.startSurvey(uniqueInteractionId, payload);

```

